#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 40

//Khai bao bien gobal
int overNumber;
int *sumArr;
int *defArr1;
int *defArr2;

//cong 2 mang
int *addNum(int arr1[], int arr2[]){
    int *resArr = (int *)malloc(MAX*sizeof(int));
    memset(resArr, 0, 160);
    int check;
    for(int i = 0; i < MAX; i++){
        check = arr1[i] + arr2[i];
        if(check > 9){
            resArr[i] += check%10;
            resArr[i+1] += 1;
        }
        else resArr[i] += check;
        if(i == MAX-1 && check > 9){
            overNumber = 1;
            return arr2;
        }
    }
    return resArr;
}

//Display
void outputNum(int *arr){
    int check = 0;
    for(int i = MAX-1; i >= 0; i--){
        if(check == 0 && arr[i] == 0) continue;
        else{
            printf("%d", arr[i]);
            check = 1;
        }
    }
    printf("\n");
}

//Fibo o day dung for, vi de quy se bi overstack neu n qua lon
int *fiboNum(int n){
    int *arr1 = defArr1;
    int *arr2 = defArr2;
    for(int i = 3; i <= n; i++){
        sumArr = addNum(arr1, arr2);  
        arr1 = arr2;
        arr2 = sumArr;  
    }
    return sumArr;
}

void menuFunc(int choose){
    long int n;
    switch(choose){
        case 1:
            printf("Nhap n: ");
            scanf("%li", &n);
            printf("Thu tu \t\t Gia tri\n");
            if(n == 1){
                printf("1 \t-\t0\n");
            }
            else{
                printf("1 \t-\t0\n");
                printf("2 \t-\t1\n");
            }
            for(int i = 3; i <=n; i++){
                printf("%d \t-\t", i);
                sumArr = fiboNum(i);
                outputNum(sumArr);
            }
            break;
        case 2:
            printf("Nhap n: ");
            scanf("%li", &n);
            if(n > 1000) n = 200; //do vuot qua gioi han 40 chu so
            sumArr = fiboNum(n);
            if(overNumber == 1){
                printf("Vuot qua gioi han!\n");
                printf("So cuoi cua day so la: ");
                outputNum(sumArr);
            }
            else{
                printf("So Fibonacy thu %d la: ", n);
                if(n == 1) printf("0");
                else if(n == 2) printf("1");
                else outputNum(sumArr);
            }
            break;
        case 3:
            exit(0);
    }
}

int main(){
    //Init
    sumArr = (int *)malloc(MAX*sizeof(int));
    defArr1 = (int *)malloc(MAX*sizeof(int));
    defArr2 = (int *)malloc(MAX*sizeof(int));
    memset(defArr1, 0, 160);
    memset(defArr2, 0, 160);
    defArr2[0] = 1; 
    
    //Menu
    do{
    overNumber = 0;
    printf("[1]. In toan bo day so tu 1 den n\n");
    printf("[2]. In gia tri so Fibonacy tai n\n");
    printf("[3]. Thoat chuong trinh\n");
    int choose;
    do{
        printf("Nhap chuc nang: ");
        scanf("%d", &choose);
    }while(choose < 1 || choose > 3 );
    menuFunc(choose);
    }while(1);
    return 0;
}